

import socketserver


class EchoHandler(socketserver.DatagramRequestHandler):
    def handle(self):
        self.wfile.write(b"Hemos recibido tu peticion")
        while 1:

            line = self.rfile.read()
            print("El cliente nos manda " + line.decode('utf-8'))

            if not line:
                break


if __name__ == "__main__":

    serv = socketserver.UDPServer(('', 6001), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    serv.serve_forever()
